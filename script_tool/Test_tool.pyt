# -*- coding: utf-8 -*-

import arcpy


class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Test Toolbox"
        self.alias = ""

        # List of tool classes associated with this toolbox
        self.tools = [Tool]


class Tool(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Test Tool"
        self.description = ""
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""

        param_accu = arcpy.Parameter(
            name="accumulation",
            displayName="Accumulation raster",
            parameterType="Required",
            direction="Input",
            datatype="DERasterDataset",
            multiValue=False)

        param_pour = arcpy.Parameter(
            name="pourpoint",
            displayName="Pour point data",
            parameterType="Required",
            direction="Input",
            datatype="GPFeatureLayer",
            multiValue=False)

        param_out = arcpy.Parameter(
            name="poursnapped",
            displayName="Output",
            parameterType="Required",
            direction="Output",
            datatype="DERasterDataset",
            multiValue=False)

        params = [param_accu, param_pour, param_out]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, params, messages):
        """The source code of the tool."""
        accumulation = params[0].value
        pour_point = params[1].value
        output = params[2].value

        arcpy.CheckOutExtension('Spatial')
        arcpy.CheckOutExtension('3D')

        arcpy.env.overwriteOutput = True
        arcpy.env.cellSize = 1
        arcpy.env.snapRaster = accumulation

        arcpy.AddMessage('Snapping pour point...')
        snap_inmem = r'in_memory\Snappoint'
        snapped = arcpy.sa.SnapPourPoint(pour_point, accumulation, 2)
        snapped.save(snap_inmem)
        arcpy.CopyRaster_management(snap_inmem, output)
        arcpy.AddMessage('Point snapped')

        return output
